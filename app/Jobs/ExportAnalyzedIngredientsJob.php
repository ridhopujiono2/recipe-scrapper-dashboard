<?php

namespace App\Jobs;

use App\Exports\AnalyzedIngredientsExport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ExportAnalyzedIngredientsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ingredientIds;
    public function __construct($ingredientIds)
    {
        $this->ingredientIds = $ingredientIds;
    }

    public function handle()
    {
        $data = [];

        foreach ($this->ingredientIds as $id) {
            // Query ke Database
            $ingredientData = DB::table('ingredients as i')
                ->join('ingredient_nutritions as inu', 'i.id', '=', 'inu.ingredient_id')
                ->join('ingredient_nutrition_details as ind', 'inu.id', '=', 'ind.ingredient_nutrition_id')
                ->join('master_nutritions as mn', 'ind.attr_id', '=', 'mn.attr_id')
                ->select('i.id as ingredient_id', 'i.description as ingredient_name', 'i.unit', 'i.value as ingredient_value', 'mn.nutrition_name', 'ind.value as nutrition_value', 'ind.attr_id')
                ->where('i.id', $id)
                ->get();

            if ($ingredientData->isEmpty()) {
                return response()->json(['message' => 'Ingredient not found or has no nutrition data'], 404);
            }

            // Ambil description dari hasil query
            $description = "{$ingredientData[0]->ingredient_value} {$ingredientData[0]->unit} {$ingredientData[0]->ingredient_name}";
            $translate = Http::get(
                config('services.api_translate.url') . "/translate",
                [
                    'keyword' => $description,
                ]
            );
            $translatedIngredient = $translate->json();

            if (stripos($translatedIngredient, 'cloves of garlic') !== false) {
                $translatedIngredient = "{$ingredientData[0]->ingredient_value} clove of garlic";
            } elseif (stripos($translatedIngredient, 'cloves of red onion') !== false || stripos($translatedIngredient, 'cloves of shallot') !== false || stripos($translatedIngredient, 'cloves of shallots') !== false || stripos($translatedIngredient, 'clove of shallot') !== false || stripos($translatedIngredient, 'clove of shallots') !== false || stripos($translatedIngredient, 'red onion') !== false) {
                $translatedIngredient = "{$ingredientData[0]->ingredient_value} shallots";
            }

            $headersNutritionix = [
                'x-app-id' => '5df38737',
                'x-app-key' => 'c2ea7fa2ed43e6dd5a5a866314cdf495',
                'Content-Type' => 'application/json'
            ];

            $bodyNutritionix = ['query' => $translatedIngredient];
            $nutritionixResponse = Http::withHeaders($headersNutritionix)->post('https://trackapi.nutritionix.com/v2/natural/nutrients', $bodyNutritionix);

            $nutritionixData = $nutritionixResponse->json();
            $nutritionixWeight = $nutritionixData['foods'][0]['serving_weight_grams'] ?? 0;
            $nutritionixNutrients = $nutritionixData['foods'][0]['full_nutrients'] ?? [];

            // Panggil Edamam API
            $edamamApiKey = 'c1bd8d9ddd308c755279a7f1b27f8eda';
            $response = Http::get("https://api.edamam.com/api/nutrition-data", [
                'app_id' => '0eb2d889',
                'app_key' => $edamamApiKey,
                'ingr' => $translatedIngredient,
            ]);

            if ($response->failed()) {
                return response()->json(['message' => 'Failed to fetch data from Edamam API'], 500);
            }

            $edamamData = $response->json();
            $edamamWeight = $edamamData['totalWeight'] ?? 0;
            $edamamNutrients = $edamamData['totalNutrients'] ?? [];

            // Buat kamus untuk memetakan attr_id ke key Edamam
            $attrToEdamamKey = [
                208 => 'ENERC_KCAL',
                204 => 'FAT',
                606 => 'FASAT',
                605 => 'FATRN',
                645 => 'FAMS',
                646 => 'FAPU',
                307 => 'NA',
                205 => 'CHOCDF',
                291 => 'FIBTG',
                269 => 'SUGAR',
                601 => 'CHOLE',
                203 => 'PROCNT',
                328 => 'VITA_RAE',
                301 => 'CA',
                303 => 'FE',
                306 => 'K',
            ];

            // Samakan nilai nutrisi berdasarkan berat
            $adjustedNutrients = [];
            foreach ($edamamNutrients as $key => $nutrient) {
                $attrId = array_search($key, $attrToEdamamKey);
                if ($attrId !== false) {
                    $nutritionixNutrient = collect($nutritionixNutrients)->firstWhere('attr_id', $attrId);
                    if ($nutritionixNutrient && $nutritionixWeight != 0) {
                        $adjustedValue = ($nutrient['quantity'] * $nutritionixWeight) / $edamamWeight;
                        $adjustedNutrients[$key] = [
                            'label' => $nutrient['label'],
                            'quantity' => $adjustedValue,
                            'unit' => $nutrient['unit'],
                        ];
                    }
                }
            }
            // Map hasil Query dengan hasil API Edamam yang disesuaikan
            $mappedData = $ingredientData->map(function ($item) use ($adjustedNutrients, $attrToEdamamKey) {
                $edamamKey = $attrToEdamamKey[$item->attr_id] ?? null;
                if ($edamamKey && isset($adjustedNutrients[$edamamKey])) {
                    $nutrient = $adjustedNutrients[$edamamKey];
                    $difference = $nutrient['quantity'] - $item->nutrition_value;
                    $percentDifference = $item->nutrition_value != 0 ? ($difference / $item->nutrition_value) * 100 : 0;
                    return [
                        'ingredient_id' => $item->ingredient_id,
                        'ingredient_name' => "{$item->ingredient_value} {$item->unit} {$item->ingredient_name}",
                        'nutrition_name' => "$item->nutrition_name",
                        'local_nutrition_value' => $item->nutrition_value,
                        'edamam_nutrition_value' => $nutrient['quantity'],
                        'difference' => abs($difference),
                        'percent_difference' => abs($percentDifference),
                    ];
                }
                return [
                    'ingredient_name' => $item->ingredient_name,
                    'nutrition_name' => $item->nutrition_name,
                    'local_nutrition_value' => $item->nutrition_value,
                    'edamam_nutrition_value' => null,
                    'difference' => null,
                    'percent_difference' => null,
                ];
            });

            // Filter out null values from the mapped data
            $filteredMappedData = $mappedData->filter(function ($item) {
                return $item['edamam_nutrition_value'] !== null;
            });

            // Tambahkan hasil yang difilter ke dalam $data
            $data = array_merge($data, $this->transformNutritionData($filteredMappedData->toArray()));
        }

        $filePath = "exports/analyzed-pivot-ingredients-" . time() . ".xlsx";
        Excel::store(new AnalyzedIngredientsExport($data), $filePath, 'local');
    }

    public function transformNutritionData($mappedData)
    {
        $result = [];

        foreach ($mappedData as $data) {
            $ingredientId = $data['ingredient_id'];
            $ingredientName = $data['ingredient_name'];

            if (!isset($result[$ingredientName])) {
                $result[$ingredientName] = [
                    'ingredient_id' => $ingredientId,
                    'description' => $ingredientName,
                    'url' => "https://dash.resepnusantara.my.id/" . $ingredientId,
                    // Initializing all potential nutrition columns
                    'Energi_Edamam' => null, 'Energi_Nutritionix' => null, 'Energi_Persentase' => null,
                    'Lemak Total_Edamam' => null, 'Lemak Total_Nutritionix' => null, 'Lemak Total_Persentase' => null,
                    'Lemak Jenuh_Edamam' => null, 'Lemak Jenuh_Nutritionix' => null, 'Lemak Jenuh_Persentase' => null,
                    'Kolesterol_Edamam' => null, 'Kolesterol_Nutritionix' => null, 'Kolesterol_Persentase' => null,
                    'Natrium_Edamam' => null, 'Natrium_Nutritionix' => null, 'Natrium_Persentase' => null,
                    'Karbohidrat_Edamam' => null, 'Karbohidrat_Nutritionix' => null, 'Karbohidrat_Persentase' => null,
                    'Serat Diet Total_Edamam' => null, 'Serat Diet Total_Nutritionix' => null, 'Serat Diet Total_Persentase' => null,
                    'Gula Total_Edamam' => null, 'Gula Total_Nutritionix' => null, 'Gula Total_Persentase' => null,
                    'Protein_Edamam' => null, 'Protein_Nutritionix' => null, 'Protein_Persentase' => null,
                    'Kalsium_Edamam' => null, 'Kalsium_Nutritionix' => null, 'Kalsium_Persentase' => null,
                    'Besi_Edamam' => null, 'Besi_Nutritionix' => null, 'Besi_Persentase' => null,
                    'Kalium_Edamam' => null, 'Kalium_Nutritionix' => null, 'Kalium_Persentase' => null,
                    'Vitamin D (D2 + D3)_Edamam' => null, 'Vitamin D (D2 + D3)_Nutritionix' => null, 'Vitamin D (D2 + D3)_Persentase' => null,
                ];
            }

            $nutritionName = $data['nutrition_name'];
            // Ensure the right data goes into the correct place
            switch ($nutritionName) {
                case 'Energi':
                    $result[$ingredientName]['Energi_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Energi_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Energi_Persentase'] = $data['percent_difference'];
                    break;
                case 'Lemak Total':
                    $result[$ingredientName]['Lemak Total_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Lemak Total_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Lemak Total_Persentase'] = $data['percent_difference'];
                    break;
                case 'Lemak Jenuh':
                    $result[$ingredientName]['Lemak Jenuh_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Lemak Jenuh_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Lemak Jenuh_Persentase'] = $data['percent_difference'];
                    break;
                case 'Kolesterol':
                    $result[$ingredientName]['Kolesterol_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Kolesterol_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Kolesterol_Persentase'] = $data['percent_difference'];
                    break;
                case 'Natrium':
                    $result[$ingredientName]['Natrium_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Natrium_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Natrium_Persentase'] = $data['percent_difference'];
                    break;
                case 'Karbohidrat':
                    $result[$ingredientName]['Karbohidrat_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Karbohidrat_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Karbohidrat_Persentase'] = $data['percent_difference'];
                    break;
                case 'Serat Diet Total':
                    $result[$ingredientName]['Serat Diet Total_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Serat Diet Total_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Serat Diet Total_Persentase'] = $data['percent_difference'];
                    break;
                case 'Gula Total':
                    $result[$ingredientName]['Gula Total_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Gula Total_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Gula Total_Persentase'] = $data['percent_difference'];
                    break;
                case 'Protein':
                    $result[$ingredientName]['Protein_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Protein_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Protein_Persentase'] = $data['percent_difference'];
                    break;
                case 'Kalsium':
                    $result[$ingredientName]['Kalsium_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Kalsium_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Kalsium_Persentase'] = $data['percent_difference'];
                    break;
                case 'Besi':
                    $result[$ingredientName]['Besi_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Besi_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Besi_Persentase'] = $data['percent_difference'];
                    break;
                case 'Kalium':
                    $result[$ingredientName]['Kalium_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Kalium_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Kalium_Persentase'] = $data['percent_difference'];
                    break;
                case 'Vitamin D (D2 + D3)':
                    $result[$ingredientName]['Vitamin D (D2 + D3)_Edamam'] = $data['edamam_nutrition_value'];
                    $result[$ingredientName]['Vitamin D (D2 + D3)_Nutritionix'] = $data['local_nutrition_value'];
                    $result[$ingredientName]['Vitamin D (D2 + D3)_Persentase'] = $data['percent_difference'];
                    break;
                default:
                    // Handle unknown nutrition names or log them for debugging purposes
                    break;
            }
        }

        return array_values($result);
    }
}

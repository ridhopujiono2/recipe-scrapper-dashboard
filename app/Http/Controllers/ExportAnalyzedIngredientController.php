<?php

namespace App\Http\Controllers;

use App\Jobs\ExportAnalyzedIngredientsJob;

class ExportAnalyzedIngredientController extends Controller
{
    public function __invoke()
    {
        ExportAnalyzedIngredientsJob::dispatch();
    }
}

<?php

namespace App\Http\Controllers\Analyses\Sampled;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{
    public function scrapingDuration(Request $request): JsonResponse
    {
        $query = DB::table('scraping_logs as sl')
            ->join('recipes as r', 'sl.recipe_id', '=', 'r.id')
            ->join('sampled_recipes as sr', 'sr.recipe_id', '=', 'r.id')
            ->select(
                'sr.id as sampled_recipe_id',
                'r.id as recipe_id',
                'r.title',
                DB::raw("IF(r.source LIKE '%resepmamiku.com%', 'resepmamiku', 'masakapahariini') as source_label"),
                DB::raw("(UNIX_TIMESTAMP(sl.end_date) - UNIX_TIMESTAMP(sl.start_date)) AS duration_seconds")
            )
            ->where('sl.status', 'success')
            ->whereRaw('(UNIX_TIMESTAMP(sl.end_date) - UNIX_TIMESTAMP(sl.start_date))');

        if ($request->paginate) {
            return new JsonResponse([
                'data' => $query->paginate($request->paginate) ?? []
            ]);
        }

        return new JsonResponse([
            'data' => $query->get()
        ]);
    }
    public function scrapingAverageDuration(Request $request): JsonResponse
    {
        $subquery = DB::table('scraping_logs as sl')
            ->join('recipes as r', 'sl.recipe_id', '=', 'r.id')
            ->join('sampled_recipes as sr', 'sr.recipe_id', '=', 'r.id')
            ->select('r.title', DB::raw('(UNIX_TIMESTAMP(sl.end_date) - UNIX_TIMESTAMP(sl.start_date)) AS duration_seconds'))
            ->where('sl.status', 'success');

        $averageDuration = DB::query()
            ->fromSub($subquery, 'sub')
            ->selectRaw('AVG(sub.duration_seconds) as average_duration')
            ->value('average_duration');

        return new JsonResponse([
            'data' => $averageDuration
        ]);
    }
    public function scrapingDurationChart(Request $request): JsonResponse
    {
        $query = DB::table('scraping_logs as sl')
            ->join('recipes as r', 'sl.recipe_id', '=', 'r.id')
            ->join('sampled_recipes as sr', 'sr.recipe_id', '=', 'r.id')
            ->select(
                DB::raw("(UNIX_TIMESTAMP(sl.end_date) - UNIX_TIMESTAMP(sl.start_date)) AS duration"),
                DB::raw("COUNT(*) AS count")
            )
            ->where('sl.status', 'success')
            // ->whereRaw('(UNIX_TIMESTAMP(sl.end_date) - UNIX_TIMESTAMP(sl.start_date)) <= 5')
            ->groupBy('duration')
            ->orderBy('duration', 'asc')
            ->get();

        // Convert data for Chart.js
        $duration = $query->pluck('duration')->all();
        $count = $query->pluck('count')->all();
        return new JsonResponse([
            'data' => [
                "count" => $count,
                "duration" => $duration,
            ]
        ]);
    }
    public function recipeIngredientAnalyzed(Request $request): JsonResponse
    {
        $query = DB::table('recipes')
            ->join('ingredients', 'recipes.id', '=', 'ingredients.recipe_id')
            ->join('sampled_recipes', 'recipes.id', '=', 'sampled_recipes.recipe_id')
            ->select(
                'recipes.id',
                'recipes.title',
                'recipes.source',
                DB::raw('COUNT(ingredients.id) AS total_ingredients'),
                DB::raw('SUM(CASE WHEN ingredients.analyzed THEN 1 ELSE 0 END) AS analyzed_ingredients'),
                DB::raw('(SUM(CASE WHEN ingredients.analyzed THEN 1 ELSE 0 END) / COUNT(ingredients.id) * 100) AS percentage_analyzed')
            )
            ->groupBy('recipes.id', 'recipes.title', 'recipes.source');

        if ($request->paginate) {
            return new JsonResponse([
                'data' => $query->paginate($request->paginate) ?? []
            ]);
        }
        return new JsonResponse([
            'data' => $query->get()
        ]);
    }
}

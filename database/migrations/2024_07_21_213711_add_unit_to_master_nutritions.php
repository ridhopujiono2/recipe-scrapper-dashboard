<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('master_nutritions', function (Blueprint $table) {
            $table->string('unit')->after('nutrition_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('master_nutritions', function (Blueprint $table) {
            $table->dropColumn(['unit']);
        });
    }
};

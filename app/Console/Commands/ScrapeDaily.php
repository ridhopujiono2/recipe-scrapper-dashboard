<?php

namespace App\Console\Commands;

use App\Http\Controllers\ScrapingController;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class ScrapeDaily extends Command
{
    protected $signature = 'scrape:daily {site} {max_limit=5} {max_thread=10}';
    protected $description = 'Execute daily recipe scraping for a specific site';

    public function handle()
    {
        $site = $this->argument('site');
        $max_limit = $this->argument('max_limit');
        $max_thread = $this->argument('max_thread');

        $request = new Request([
            'site' => $site,
            'max_limit' => (int) $max_limit,
            'max_thread' => (int) $max_thread
        ]);

        $controller = new ScrapingController();
        $controller($request);
        $this->info("Scraping executed successfully for $site.");
    }
}

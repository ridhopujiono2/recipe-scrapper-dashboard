import Axios from 'axios';

const baseURL = import.meta.env?.VITE_APP_URL || 'https://dash.resepnusantara.my.id';

export default Axios.create({
  baseURL,
  withCredentials: true,
  headers: {
    Authorization: `Bearer ${localStorage.getItem('token')}`,
    'X-Requested-With': 'XMLHttpRequest',
  },
});

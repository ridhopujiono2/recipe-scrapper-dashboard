import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

declare global {
  interface Window {
    gtag?: (key: string, trackingId: string, config: { page_path: string }) => void;
  }
}

export const useGoogleTag = (trackingId: string | undefined = 'G-Y63M1J8322') => {
  const location = useLocation();

  useEffect(() => {
    if (!window.gtag) return;
    if (!trackingId) {
      console.log('Tracking not enabled, as `trackingId` was not given and there is no `GTAG_ID`.');
      return;
    }

    // console.log('tracking', location);

    window.gtag('config', trackingId, { page_path: location.pathname });
  }, [window.gtag, trackingId, location]);
};

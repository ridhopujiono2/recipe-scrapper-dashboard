import React, { useState, useEffect } from 'react';
import { Card, Grid, Text, Image, Pagination, TextInput, Button, Skeleton, Flex } from '@mantine/core';
import { api } from '../../lib';
import { IconSearch } from '@tabler/icons-react';

const RecipesList = () => {
    const [recipes, setRecipes] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [lastPage, setLastPage] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');

    const fetchRecipes = async (page = 1, query = '') => {
        setIsLoading(true);
        try {
            const response = await api.recipes.list({ paginate: 20, page, recipe_name: query });
            setRecipes(response?.data?.data?.data);
            setCurrentPage(response?.data?.data?.current_page);
            setLastPage(response?.data?.data?.last_page);
        } catch (error) {
            console.error('Error fetching recipes:', error);
        } finally {
            setIsLoading(false);
        }
    };

    useEffect(() => {
        fetchRecipes(currentPage, searchQuery);
    }, [currentPage]);

    const handlePageChange = (page) => {
        setCurrentPage(page);
    };

    const handleSearchSubmit = () => {
        fetchRecipes(1, searchQuery);
    };

    function removeSizeFromUrl(url) {
        return url.replace(/-\d+x\d+(?=\.\w+$)/, '');
    }

    function isYoutubeUrl(url) {
        return url.includes('youtube.com') || url.includes('youtu.be');
    }

    function getYoutubeEmbedUrl(url) {
        const videoId = url.split('v=')[1]?.split('&')[0] || url.split('youtu.be/')[1]?.split('?')[0];
        return `https://www.youtube.com/embed/${videoId}`;
    }

    return (
        <Flex p={20} direction="column">
            <div style={{ display: 'flex', alignItems: 'center', marginBottom: '50px' }}>
                <TextInput
                    size="lg"
                    placeholder="Cari resep..."
                    value={searchQuery}
                    onChange={(event) => setSearchQuery(event.currentTarget.value)}
                    onKeyPress={(event) => {
                        if (event.key === 'Enter') {
                            handleSearchSubmit();
                        }
                    }}
                    style={{ width: '100%', marginRight: '10px' }}
                />
                <Button size="lg" onClick={handleSearchSubmit} disabled={isLoading}>
                    Cari
                </Button>
            </div>

            <Grid>
                {isLoading
                    ? Array.from({ length: 8 }).map((_, index) => (
                        <Grid.Col key={index} span={12} md={6} lg={4} xl={3}>
                            <Card shadow="sm" padding="lg">
                                <Skeleton height={300} mb="md" />
                                <Skeleton height={20} width="70%" mb="sm" />
                            </Card>
                        </Grid.Col>
                    ))
                    : recipes.map((recipe) => (
                        <Grid.Col key={recipe?.id} span={12} md={6} lg={4} xl={3}>
                            <a href={`/${recipe?.id}`} style={{ textDecoration: 'none' }}>
                                <Card shadow="sm" padding="lg">
                                    <Card.Section style={{ position: 'relative', height: 300, overflow: 'hidden' }}>
                                        {isYoutubeUrl(recipe?.image) ? (
                                            <iframe
                                                src={getYoutubeEmbedUrl(recipe?.image)}
                                                title="YouTube video player"
                                                frameBorder="0"
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowFullScreen
                                                style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%' }}
                                            ></iframe>
                                        ) : (
                                            <Image src={removeSizeFromUrl(recipe?.image)} alt={recipe.title} withPlaceholder fit="cover" style={{ width: '100%', height: '100%' }} />
                                        )}
                                    </Card.Section>
                                    <Text weight={500} size="lg" mt="md">
                                        {recipe?.title}
                                    </Text>
                                    <Text size="sm" color="dimmed">
                                        {recipe?.description}
                                    </Text>
                                </Card>
                            </a>
                        </Grid.Col>
                    ))}
                <Grid.Col span={12}>
                    <Pagination page={currentPage} onChange={handlePageChange} total={lastPage} value={currentPage} color="blue" size="md" radius="md" />
                </Grid.Col>
            </Grid>
        </Flex>
    );
};

export default RecipesList;

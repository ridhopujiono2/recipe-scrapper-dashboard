import React from 'react';
import { MantineProvider } from '@mantine/core';

const App: React.FC = () => {
    return (
        <MantineProvider withGlobalStyles withNormalizeCSS>
            <div>
                <h1>Hello, React with Mantine and TypeScript!</h1>
            </div>
        </MantineProvider>
    );
};

export default App;

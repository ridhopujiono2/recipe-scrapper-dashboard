import React from 'react';
import { Footer as MantineFooter, Text } from '@mantine/core';

const Footer: React.FC = () => {
    return (
        <MantineFooter height={60} p="xs">
            <Text>Footer content</Text>
        </MantineFooter>
    );
};

export default Footer;

<?php

namespace App\Jobs;

use App\Models\Recipe;
use App\Models\SampledRecipe;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SampleRecipesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct()
    {
        //
    }

    public function handle()
    {
        $sourceMasakApaHariIni = Recipe::where('source', 'like', '%masakapahariini.com%');
        $sourceResepMamiku = Recipe::where('source', 'like', '%resepmamiku.com%');

        $sourceACount = $sourceMasakApaHariIni->count();
        $sourceBCount =  $sourceResepMamiku->count();

        $totalRecipes = $sourceACount + $sourceBCount;

        $totalSampleSize = calculateCochranSampleSize('95%', 0.5, 0.05, $totalRecipes);

        $sampleFromA = round(($sourceACount / $totalRecipes) * $totalSampleSize);
        $sampleFromB = $totalSampleSize - $sampleFromA;

        // Mengambil sampel dari sumber A dan B
        $sampledA = $sourceMasakApaHariIni
            ->inRandomOrder()
            ->limit($sampleFromA)
            ->pluck('id');  // Ambil hanya ID

        $sampledB = $sourceResepMamiku
            ->inRandomOrder()
            ->limit($sampleFromB)
            ->pluck('id');  // Ambil hanya ID

        // Menyimpan sampel ke dalam tabel SampledRecipes
        $allSampledIds = $sampledA->merge($sampledB)->shuffle();

        DB::table('sampled_recipes')->truncate();

        foreach ($allSampledIds as $recipeId) {
            SampledRecipe::create(['recipe_id' => $recipeId]);
        }
    }
}

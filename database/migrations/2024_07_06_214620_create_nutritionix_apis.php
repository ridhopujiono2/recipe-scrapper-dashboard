<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('nutritionix_apis', function (Blueprint $table) {
            $table->id();
            $table->string('x_app_id');
            $table->string('x_app_key');
            $table->boolean('reach_limit')->default(false);
            $table->dateTime('has_been_reached_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('nutritionix_apis');
    }
};

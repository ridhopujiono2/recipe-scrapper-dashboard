<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recipe;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class CheckInsertedRecipeController extends Controller
{
    public function __invoke(Request $request): JsonResponse
    {
        $request->validate([
            'created_at' => 'required',
            'total' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $recipe = Recipe::where('created_at', '>=', $request->created_at)->count();
            if ($recipe === 0) {
                DB::commit();
                return new JsonResponse(['data' => 0], 200);
            }

            $calculatedPercent = ($recipe / (int) $request->total) * 100;
            DB::commit();
            return new JsonResponse(['data' => $calculatedPercent], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return new JsonResponse(['message' => $e->getMessage()], 500);
        }
    }
}

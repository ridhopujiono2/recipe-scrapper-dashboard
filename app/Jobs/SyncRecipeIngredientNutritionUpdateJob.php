<?php

namespace App\Jobs;

use App\Models\IngredientNutrition;
use App\Models\IngredientNutritionDetail;
use App\Models\Recipe;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SyncRecipeIngredientNutritionUpdateJob implements ShouldQueue, ShouldBeUnique
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $allowedAttrId = [
        208,
        204,
        606,
        605,
        646,
        545,
        307,
        205,
        291,
        269,
        601,
        203,
        328,
        301,
        303,
        306
    ];
    /**
     * Create a new job instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Log::info('Running SyncRecipeIngredientNutritionUpdateJob for ingredient ID: ' . $this->data->id);
        try {
            $ingredient = $this->data;
            $translate = Http::get(
                config('services.api_translate.url') . "/translate",
                [
                    'keyword' => "$ingredient->value $ingredient->unit $ingredient->description",
                ]
            );
            Log::info('Translation API response', ['status' => $translate->status(), 'body' => $translate->body()]);

            if ($translate->successful()) {
                $translatedIngredient = $translate->json();

                // Check for specific phrases and replace accordingly
                if (stripos($translatedIngredient, 'cloves of garlic') !== false) {
                    $translatedIngredient = "$ingredient->value clove of garlic";
                } elseif (stripos($translatedIngredient, 'cloves of red onion') !== false || stripos($translatedIngredient, 'cloves of shallot') !== false || stripos($translatedIngredient, 'cloves of shallots') !== false || stripos($translatedIngredient, 'clove of shallot') !== false || stripos($translatedIngredient, 'clove of shallots') !== false || stripos($translatedIngredient, 'red onion') !== false) {
                    $translatedIngredient = "$ingredient->value shallots";
                }
                $nutritions = $this->getNutritions($translatedIngredient);

                if ($nutritions['status'] == 200) {
                    foreach ($nutritions['data']['foods'] as $nutrition) {
                        $ingredientNutrition = IngredientNutrition::updateOrCreate(
                            [
                                'ingredient_id' => $ingredient['id'],
                                'food_name' => $nutrition['food_name']
                            ]
                        );
                        foreach ($nutrition['full_nutrients'] as $fullNutrient) {
                            if (in_array($fullNutrient['attr_id'], $this->allowedAttrId)) {
                                $bulkInsertData[] = [
                                    'ingredient_nutrition_id' => $ingredientNutrition->id,
                                    'attr_id' => $fullNutrient['attr_id'],
                                    'value' => $fullNutrient['value'],
                                ];
                            }
                        }
                    }
                    // Bulk insert
                    if (!empty($bulkInsertData)) {
                        IngredientNutritionDetail::insert($bulkInsertData);
                    }
                    $ingredient->update(['analyzed' => 1]);
                }
            }
        } catch (\Exception $e) {
            Log::error('Sync Recipe Ingredient Nutritions Data Error', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    protected function getNutritions($keyword)
    {
        // Mengambil API credentials yang belum mencapai limit
        $api = DB::table('nutritionix_apis')->where('reach_limit', false)->first();

        if (!$api) {
            Log::error('All API keys have reached their limit');
            throw new \RuntimeException("All API keys have reached their limit");
        }

        $headers = [
            'x-app-id' => $api->x_app_id,
            'x-app-key' => $api->x_app_key,
            'Content-Type' => 'application/json'
        ];

        $body = ['query' => $keyword];
        $data = Http::withHeaders($headers)->post('https://trackapi.nutritionix.com/v2/natural/nutrients', $body);

        if ($data->successful()) {
            return [
                "status" => 200,
                "data" => $data->json()
            ];
        }

        // Cek jika batas tercapai dan update database
        if ($data->getStatusCode() == 401) {
            DB::table('nutritionix_apis')->where('id', $api->id)->update(['reach_limit' => true, 'has_been_reached_at' => Date::now()]);
            // Coba mendapatkan API credentials yang lain dan retry
            return $this->getNutritions($keyword);
        }

        return [
            "status" => $data->getStatusCode(),
            "data" => $data->json(),
        ];
    }
}

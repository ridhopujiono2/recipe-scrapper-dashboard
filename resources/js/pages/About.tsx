import React from 'react';
import { Text } from '@mantine/core';

const About: React.FC = () => {
    return (
        <div>
            <Text size="xl">Welcome to About Page</Text>
        </div>
    );
};

export default About;

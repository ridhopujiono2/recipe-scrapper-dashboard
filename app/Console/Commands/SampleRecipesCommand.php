<?php

namespace App\Console\Commands;

use App\Jobs\SampleRecipesJob;
use Illuminate\Console\Command;

class SampleRecipesCommand extends Command
{
    protected $signature = 'sample:recipes';
    protected $description = 'Command to sample recipes and save them';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        dispatch(new SampleRecipesJob());
        $this->info('Sampling job dispatched.');
    }
}

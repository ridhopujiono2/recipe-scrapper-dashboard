<?php

namespace App\Http\Controllers\Analyses;

use App\Http\Controllers\Controller;
use App\Models\Recipe;
use App\Models\ScrapingLog;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{
    public function scrapedToday(Request $request): JsonResponse
    {
        $query = ScrapingLog::whereDate('start_date', Date::today())->count();
        return new JsonResponse([
            'data' => $query
        ]);
    }
}

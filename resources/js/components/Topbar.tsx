import React from 'react';
import { Header, Text } from '@mantine/core';

const Topbar: React.FC = () => {
    return (
        <Header height={60} p="xs">
            <Text>Header content</Text>
        </Header>
    );
};

export default Topbar;

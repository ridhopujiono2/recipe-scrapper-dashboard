import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Container, Image, Text, Title, List, ThemeIcon, Divider, Group, Badge, Card, Skeleton, Grid, Col, Button, Modal, ActionIcon, Flex, Tooltip } from '@mantine/core';
import { api } from '../../lib';
import { IconQuestionMark } from '@tabler/icons-react';

interface Ingredient {
    id: number;
    title: string;
    value: string | null;
    unit: string | null;
    description: string;
}

interface Step {
    id: number;
    title: string;
    step_description: string;
    image: string | null;
}

interface Category {
    id: number;
    title: string;
    link: string;
    created_at: string;
    updated_at: string;
    pivot: {
        recipe_id: number;
        category_id: number;
    };
}

interface RecipeDetail {
    id: number;
    title: string;
    code: string;
    source: string;
    author: string;
    likes: number;
    image: string;
    description: string;
    ingredients: Ingredient[];
    steps: Step[];
    categories: Category[];
    nutrition: {
        calories: string;
        fat: string;
        protein: string;
        carbs: string;
    };
}

interface NutritionDetail {
    nutrition_name: string;
    unit: string;
    total_nutrition_value: number;
}

const RecipeDetail: React.FC = () => {
    const { id } = useParams<{ id: string }>();
    const [recipe, setRecipe] = useState<RecipeDetail | null>(null);
    const [nutritionTotal, setNutritionTotal] = useState<NutritionDetail | null>(null);
    const [ingredientDetail, setIngredientDetail] = useState<Ingredient | null>(null);
    const [ingredientNutrition, setIngredientNutrition] = useState<NutritionDetail[]>([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        // Fetch data from API
        const fetchRecipe = async () => {
            try {
                const response = await api.recipes.get(Number(id));
                console.log(response);
                setRecipe(response?.data?.data);
            } catch (error) {
                console.error('Error fetching recipe:', error);
            } finally {
                setLoading(false);
            }
        };

        const fetchNutritionTotal = async () => {
            try {
                const response = await api.recipes.nutritionTotal(Number(id));
                console.log(response);
                setNutritionTotal(response?.data?.data);
            } catch (error) {
                console.error('Error fetching recipe:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchRecipe();
        fetchNutritionTotal();
    }, [id]);

    if (loading) {
        return (
            <Container size="xl">
                <Skeleton height={1000} radius="xl" />
                <Skeleton height={8} mt={6} radius="xl" />
                <Skeleton height={8} mt={6} radius="xl" />
                <Skeleton height={200} mt={6} radius="xl" />
            </Container>
        );
    }

    if (!recipe) {
        return <Text>Recipe not found</Text>;
    }

    const groupedIngredients = recipe?.ingredients?.reduce((acc: { [key: string]: Ingredient[] }, ingredient) => {
        if (!acc[ingredient?.title]) {
            acc[ingredient?.title] = [];
        }
        acc[ingredient?.title].push(ingredient);
        return acc;
    }, {});

    const groupedSteps = recipe?.steps?.reduce((acc: { [key: string]: Step[] }, step) => {
        if (!acc[step?.title]) {
            acc[step?.title] = [];
        }
        acc[step?.title].push(step);
        return acc;
    }, {});

    function removeSizeFromUrl(url: string) {
        return url.replace(/-\d+x\d+(?=\.\w+$)/, '');
    }

    function isYoutubeUrl(url: string) {
        return url.includes('youtube.com') || url.includes('youtu.be');
    }

    function getYoutubeEmbedUrl(url: string) {
        const videoId = url.split('v=')[1]?.split('&')[0] || url.split('youtu.be/')[1]?.split('?')[0];
        return `https://www.youtube.com/embed/${videoId}`;
    }

    const sections = {
        "Energi": ["Energi"],
        "Lemak": ["Lemak Total", "Lemak Jenuh", "Lemak Trans", "Kolesterol"],
        "Sodium": ["Natrium"],
        "Total Karbohidrat": ["Karbohidrat, berbeda", "Serat Diet Total", "Gula Total"],
        "Protein": ["Protein"],
        "Nutrisi Tambahan": ["Kalsium", "Besi", "Kalium", "Vitamin D (D2 + D3)"]
    };

    const fetchIngredientNutrition = async (ingredientId: number) => {
        try {
            const response = await api.recipes.getNutrition(ingredientId);
            setIngredientNutrition(response?.data?.data);
            setIsModalOpen(true);
        } catch (error) {
            console.error('Error fetching ingredient nutrition:', error);
        }
    };

    const showIngredientDetails = (ingredient: Ingredient) => {
        setIngredientDetail(ingredient);
        fetchIngredientNutrition(ingredient.id);
    };

    return (
        <Container size="xl">
            <Grid>
                <Col span={12}>
                    <Card padding="lg" radius="md" withBorder>
                        <Card.Section>
                            {isYoutubeUrl(recipe?.image) ? (
                                <div style={{ position: 'relative', paddingBottom: '56.25%', height: 0, overflow: 'hidden', maxWidth: '100%', background: '#000' }}>
                                    <iframe
                                        src={getYoutubeEmbedUrl(recipe?.image)}
                                        title="YouTube video player"
                                        frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen
                                        style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%' }}
                                    ></iframe>
                                </div>
                            ) : (
                                <Image height={500} src={removeSizeFromUrl(recipe?.image)} alt={recipe.title} withPlaceholder />
                            )}
                        </Card.Section>

                        <Group position="apart" mt="md" mb="xs">
                            <Title order={2}>{recipe?.title}</Title>
                            <Badge color="pink" variant="light">
                                {recipe?.likes} Likes
                            </Badge>
                        </Group>
                        <Text size="sm" color="dimmed">Author: {recipe?.author}</Text>

                        <Group mt="md">
                            {recipe?.categories?.map((category) => (
                                <Badge key={category.id} component="a" href={category.link} target="_blank" style={{ cursor: 'pointer' }} color="blue" variant="light">
                                    {category.title}
                                </Badge>
                            ))}
                        </Group>
                        <Divider my="sm" />
                        <Grid>
                            <Col
                                span={12}
                                lg={9}
                                sx={(theme) => ({
                                    [theme.fn.smallerThan('lg')]: {
                                        borderRight: 'none',
                                        borderBottom: '1.5px solid #ced4da',
                                    },
                                    [theme.fn.largerThan('lg')]: {
                                        borderRight: '1.5px solid #ced4da',
                                    },
                                })}>
                                {
                                    recipe?.source?.includes("resepmamiku") && (
                                        <Title order={3} mb={10}>Bahan</Title>
                                    )
                                }
                                {Object.keys(groupedIngredients).map((title, index) => (
                                    <div key={index}>
                                        {
                                            recipe?.source?.includes("masakapahariini") ? (
                                                <Title order={3} mb={10}>Bahan</Title>
                                            ) : (
                                                <Title order={4} mt="sm" mb={10}>{title}</Title>
                                            )
                                        }
                                        <List spacing="xs" size="md" withPadding>
                                            {groupedIngredients[title].map((ingredient) => (
                                                <List.Item key={ingredient.id}>
                                                    <Flex gap={10} align="center">
                                                        {ingredient.value} {ingredient.unit} {ingredient.description}
                                                        <ActionIcon size={15} variant="filled" radius="xl" aria-label="Lihat detail nutrisi">
                                                            <Tooltip
                                                                position='top'
                                                                label="Lihat Informasi Nutrisi"
                                                            >
                                                                <IconQuestionMark size={13} onClick={() => showIngredientDetails(ingredient)} />
                                                            </Tooltip>
                                                        </ActionIcon>
                                                    </Flex>
                                                </List.Item>
                                            ))}
                                        </List>
                                    </div>
                                ))}

                                {
                                    recipe?.source?.includes("resepmamiku") && (
                                        <Title order={3}>Cara membuat</Title>
                                    )
                                }

                                {Object.keys(groupedSteps).map((title, index) => (
                                    <div key={index}>
                                        {
                                            recipe?.source?.includes("masakapahariini") ? (
                                                <Title order={3} mb={10}>Cara membuat</Title>
                                            ) : (
                                                <Title order={4} mt="sm" mb={10}>{title}</Title>
                                            )
                                        }
                                        <List
                                            spacing="xs"
                                            size="md"
                                        >
                                            {groupedSteps[title].map((step, stepIndex) => (
                                                <List.Item
                                                    key={step.id}
                                                    icon={
                                                        <ThemeIcon color="teal" size={24} radius="xl">
                                                            <Text size={16} weight={700} color="white">{stepIndex + 1}</Text>
                                                        </ThemeIcon>
                                                    }
                                                >
                                                    {step.step_description}
                                                    {step.image && (
                                                        isYoutubeUrl(step.image) ? (
                                                            <iframe
                                                                width="100%"
                                                                height="315"
                                                                src={getYoutubeEmbedUrl(step.image)}
                                                                title="YouTube video player"
                                                                frameBorder="0"
                                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                                allowFullScreen
                                                                style={{ marginTop: '10px' }}
                                                            ></iframe>
                                                        ) : (
                                                            <Image width="100%" src={step.image} alt={step.title} withPlaceholder style={{ marginTop: '10px' }} />
                                                        )
                                                    )}
                                                </List.Item>
                                            ))}
                                        </List>
                                    </div>
                                ))}
                            </Col>
                            <Col span={12} lg={3}>
                                <Card padding="lg" radius="md" withBorder>
                                    <Title order={3} mb="sm">Nutrition Facts</Title>
                                    <Divider my="sm" />

                                    {Object.entries(sections).map(([section, items]) => (
                                        <>
                                            <Title order={4} mt={7}>{section}</Title>
                                            {items.map(item =>
                                                nutritionTotal?.filter(n => n.nutrition_name === item).map(nutrient => (
                                                    <Text key={nutrient?.nutrition_name} ml={10} size="sm">
                                                        {nutrient?.nutrition_name}: {parseFloat(nutrient?.total_nutrition_value).toFixed(2)} {nutrient?.unit}
                                                    </Text>
                                                ))
                                            )}
                                        </>
                                    ))}
                                    <Divider my={10} />
                                    <Text size="sm" style={{ fontStyle: 'italic' }}>By Nutritionix.com</Text>
                                </Card>
                            </Col>
                        </Grid>
                        <Divider my="sm" />
                        Sumber: {recipe?.source}
                    </Card>
                </Col >
            </Grid >
            <Modal
                opened={isModalOpen}
                onClose={() => setIsModalOpen(false)}
                title={ingredientDetail ? `Nutrition Facts untuk ${ingredientDetail.title}` : 'Nutrition Facts'}
                size="sm"
            >
                {ingredientNutrition.length > 0 ? (

                    <Card padding="lg" radius="md" withBorder>
                        <Title order={3} mb="sm">Nutrition Facts</Title>
                        <Divider my="sm" />

                        {Object.entries(sections).map(([section, items]) => (
                            <>
                                <Title order={4} mt={7}>{section}</Title>
                                {items.map(item =>
                                    ingredientNutrition?.filter(n => n.nutrition_name === item).map(nutrient => (
                                        <Text key={nutrient?.nutrition_name} ml={10} size="sm">
                                            {nutrient?.nutrition_name}: {parseFloat(nutrient?.total_nutrition_value).toFixed(2)} {nutrient?.unit}
                                        </Text>
                                    ))
                                )}
                            </>
                        ))}

                        <Divider my={10} />
                        <Text size="sm" style={{ fontStyle: 'italic' }}>By Nutritionix.com</Text>
                    </Card>
                ) : (
                    <Text>Empty Data ...</Text>
                )}
            </Modal>
        </Container >

    );
};

export default RecipeDetail;

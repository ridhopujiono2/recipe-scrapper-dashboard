<?php

namespace App\Console\Commands;

use App\Jobs\ScrapeRecipe;
use App\Models\Recipe;
use Illuminate\Console\Command;

class ScrapingRecipes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:scraping-recipes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scraping Recipes';

    public function latestRecipe()
    {
        $data = Recipe::orderBy('created_on_website_at', 'desc')->first();
        return [
            $data ? $data->code : null,
            $data ? $data->page : null,
        ];
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $latestScrapedId = $this->latestRecipe()[0];
        $latestScrapedPage = $this->latestRecipe()[1];

        // Menampilkan pesan ke console
        $this->info('Starting the scraping process...');

        // Mendispatch job ke queue dengan latestScrapedId
        ScrapeRecipe::dispatch("resepmamiku.com", "https://resepmamiku.com/masakan", $latestScrapedId, $latestScrapedPage, 100, 15);

        // Menampilkan pesan bahwa job telah didispatch
        $this->info('ScrapeRecipe job has been dispatched.');
    }
}

<?php

namespace App\Console\Commands;

use App\Jobs\SyncRecipeIngredientNutritionJob;
use Illuminate\Console\Command;

class SyncRecipeIngredientNutritionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:analyses-nutrition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analyses Nutrition';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        SyncRecipeIngredientNutritionJob::dispatch();
    }
}

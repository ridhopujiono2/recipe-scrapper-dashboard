<?php

namespace App\Jobs;

use App\Models\Ingredient;
use App\Models\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;

class SyncSampledRecipeIngredientNutritionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            $batch = Bus::batch([])
                ->name('Ingredient Nutrition | Sync Sampled Recipe Ingredient Nutritions')
                ->dispatch();

            $ingredients =
                Ingredient::query()->select('ingredients.*', 'recipes.id as recipe_id', 'sampled_recipes.id as sampled_recipe_id')->join('recipes', 'ingredients.recipe_id', '=', 'recipes.id')->join('sampled_recipes', 'recipes.id', '=', 'sampled_recipes.recipe_id')->where('ingredients.analyzed', 0)
                ->get();

            foreach ($ingredients as $ingredient) {
                $batch->add([
                    new SyncRecipeIngredientNutritionUpdateJob($ingredient)
                ]);
            }
        } catch (\Exception $e) {
            Log::error('Sync Sampled Recipe Ingredient Nutritions Data Error', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }
}

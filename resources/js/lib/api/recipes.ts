import axios from "../axios";

export function list(params?: any) {
    return axios.get(`/api/recipes`, { params });
}
export function get(id: number) {
    return axios.get(`/api/recipes/${id}`);
}
export function nutritionTotal(id: number) {
    return axios.get(`/api/recipes/ingredients/nutrition-total/${id}`);
}
export function getNutrition(id: number) {
    return axios.get(`/api/recipes/ingredients/nutritions/${id}`);
}

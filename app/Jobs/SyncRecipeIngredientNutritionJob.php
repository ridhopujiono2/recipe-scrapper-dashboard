<?php

namespace App\Jobs;

use App\Models\Ingredient;
use App\Models\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;

class SyncRecipeIngredientNutritionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            $batch = Bus::batch([])
                ->name('Ingredient Nutrition | Sync Recipe Ingredient Nutritions')
                ->dispatch();

            $ingredients = Ingredient::query()
                ->where('analyzed', 0)
                ->limit(4000)
                ->get();

            foreach ($ingredients as $ingredient) {
                $batch->add([
                    new SyncRecipeIngredientNutritionUpdateJob($ingredient)
                ]);
            }
        } catch (\Exception $e) {
            Log::error('Sync Recipe Ingredient Nutritions Data Error', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }
}

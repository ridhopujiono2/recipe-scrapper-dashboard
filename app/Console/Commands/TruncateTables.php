<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Recipe;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\ScrapingLog;
use App\Models\Step;

class TruncateTables extends Command
{
    protected $signature = 'truncate:tables';

    protected $description = 'Truncate recipes, scraping logs, categories, ingredients, and steps tables';

    public function handle()
    {
        // Memastikan operasi truncate diizinkan
        if ($this->confirm('Apakah Anda yakin ingin mengosongkan tabel-tabel ini? Ini tidak dapat dibatalkan.')) {
            // Menonaktifkan foreign key checks untuk mengizinkan truncate pada tabel dengan foreign key
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            Recipe::truncate();
            Category::truncate();
            DB::table('recipe_categories')->truncate(); // Untuk tabel pivot
            Ingredient::truncate();
            Step::truncate();
            ScrapingLog::truncate();

            // Mengaktifkan kembali foreign key checks
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            $this->info('Tabel-tabel berhasil dikosongkan.');
        }
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class AnalyzedIngredientsExport implements FromCollection, WithHeadings, WithCustomStartCell, WithEvents
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function startCell(): string
    {
        return 'A2';
    }

    public function headings(): array
    {
        return [
            'ingredient_id', 'description', 'url',
            'Energi_Edamam', 'Energi_Nutritionix', 'Energi_Persentase',
            'Lemak Total_Edamam', 'Lemak Total_Nutritionix', 'Lemak Total_Persentase',
            'Lemak Jenuh_Edamam', 'Lemak Jenuh_Nutritionix', 'Lemak Jenuh_Persentase',
            'Kolesterol_Edamam', 'Kolesterol_Nutritionix', 'Kolesterol_Persentase',
            'Natrium_Edamam', 'Natrium_Nutritionix', 'Natrium_Persentase',
            'Karbohidrat_Edamam', 'Karbohidrat_Nutritionix', 'Karbohidrat_Persentase',
            'Serat Diet Total_Edamam', 'Serat Diet Total_Nutritionix', 'Serat Diet Total_Persentase',
            'Gula Total_Edamam', 'Gula Total_Nutritionix', 'Gula Total_Persentase',
            'Protein_Edamam', 'Protein_Nutritionix', 'Protein_Persentase',
            'Kalsium_Edamam', 'Kalsium_Nutritionix', 'Kalsium_Persentase',
            'Besi_Edamam', 'Besi_Nutritionix', 'Besi_Persentase',
            'Kalium_Edamam', 'Kalium_Nutritionix', 'Kalium_Persentase',
            'Vitamin D (D2 + D3)_Edamam', 'Vitamin D (D2 + D3)_Nutritionix', 'Vitamin D (D2 + D3)_Persentase',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();

                // Merge cells for parent headers
                $sheet->mergeCells('D1:F1');
                $sheet->setCellValue('D1', 'Energi');

                $sheet->mergeCells('G1:I1');
                $sheet->setCellValue('G1', 'Lemak Total');

                $sheet->mergeCells('J1:L1');
                $sheet->setCellValue('J1', 'Lemak Jenuh');

                $sheet->mergeCells('M1:O1');
                $sheet->setCellValue('M1', 'Kolesterol');

                $sheet->mergeCells('P1:R1');
                $sheet->setCellValue('P1', 'Natrium');

                $sheet->mergeCells('S1:U1');
                $sheet->setCellValue('S1', 'Karbohidrat');

                $sheet->mergeCells('V1:X1');
                $sheet->setCellValue('V1', 'Serat Diet Total');

                $sheet->mergeCells('Y1:AA1');
                $sheet->setCellValue('Y1', 'Gula Total');

                $sheet->mergeCells('AB1:AD1');
                $sheet->setCellValue('AB1', 'Protein');

                $sheet->mergeCells('AE1:AG1');
                $sheet->setCellValue('AE1', 'Kalsium');

                $sheet->mergeCells('AH1:AJ1');
                $sheet->setCellValue('AH1', 'Besi');

                $sheet->mergeCells('AK1:AM1');
                $sheet->setCellValue('AK1', 'Kalium');

                $sheet->mergeCells('AN1:AP1');
                $sheet->setCellValue('AN1', 'Vitamin D (D2 + D3)');
            }
        ];
    }
}

<?php

use App\Http\Controllers\AnalyzedIngredientNutritionController;
use App\Http\Controllers\RecipeController;
use App\Http\Controllers\CheckInsertedRecipeController;
use App\Http\Controllers\Analyses\Sampled\RecipeController as SampledRecipeController;
use App\Http\Controllers\Analyses\RecipeController as AnalyzesRecipeController;
use App\Http\Controllers\ExportAnalyzedIngredientController;
use App\Http\Controllers\IngredientNutritionController;
use App\Http\Controllers\ScrapingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::resource('recipes', RecipeController::class);
Route::resource('recipes/ingredients', IngredientNutritionController::class);
Route::get('recipes/ingredients/nutritions/{id}', [IngredientNutritionController::class, 'showNutritions']);
Route::get('recipes/ingredients/nutrition-total/{recipeId}', [IngredientNutritionController::class, 'showNutritionTotal']);

Route::post('recipes/ingredients/nutrition-validity', [IngredientNutritionController::class, 'showDetailNutrition']);

Route::post('/scraping-recipe', ScrapingController::class);
Route::get('/check-inserted-recipe', CheckInsertedRecipeController::class);
Route::get('/analyzed-ingredient-exports', ExportAnalyzedIngredientController::class);

// Analyze
Route::get('/recipes/analyzed', [AnalyzedIngredientNutritionController::class, 'index']);
Route::get('/recipes/scraped-today', [AnalyzesRecipeController::class, 'scrapedToday']);

Route::get('/sampled-recipe/scraping-duration', [SampledRecipeController::class, 'scrapingDuration']);
Route::get('/sampled-recipe/scraping-duration-avg', [SampledRecipeController::class, 'scrapingAverageDuration']);
Route::get('/sampled-recipe/scraping-duration-chart', [SampledRecipeController::class, 'scrapingDurationChart']);
Route::get('/sampled-recipe/scraping-ingredient-analyzed', [SampledRecipeController::class, 'recipeIngredientAnalyzed']);

<?php

namespace App\Http\Controllers;

use App\Jobs\ScrapeRecipe;
use Illuminate\Http\Request;
use App\Models\Recipe;

class ScrapingController extends Controller
{
    public $RECIPE_URL = [
        "resepmamiku.com" => "https://resepmamiku.com/masakan",
        "masakapahariini.com" => "https://www.masakapahariini.com/resep/",
    ];

    public function __invoke(Request $request)
    {
        try {

            $latestData = $this->latestRecipe($request->site);
            $latestScrapedId = $latestData[0];
            $latestScrapedPage = $latestData[1];

            // // Mendispatch job ke queue dengan latestScrapedId
            ScrapeRecipe::dispatch($request->site, $this->RECIPE_URL[$request->site], $latestScrapedId, $latestScrapedPage, (int) $request->max_limit, (int) $request->max_thread);

            return response()->json(['data' => 'Sedang melakukan scraping'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function latestRecipe($site)
    {
        $data =  Recipe::when($site, fn ($query) => $query->where('source', 'like', "%$site%"))
            ->orderBy('created_on_website_at', 'asc')->first();
        return [
            $data ? $data->code : null,
            $data ? $data->page : null,
        ];
    }
}

<?php

function calculateCochranSampleSize($confidenceLevel, $p, $e, $N = null)
{
    // Mengubah tingkat kepercayaan menjadi nilai z
    $zValues = [
        '90%' => 1.645, // Z value for 90% confidence
        '95%' => 1.96,  // Z value for 95% confidence
        '99%' => 2.576  // Z value for 99% confidence
    ];

    $Z = $zValues[$confidenceLevel] ?? null;
    if (!$Z) {
        throw new Exception("Invalid confidence level. Choose 90%, 95%, or 99%");
    }

    // Cochran's formula untuk ukuran sampel awal
    $n0 = pow($Z, 2) * $p * (1 - $p) / pow($e, 2);

    // Jika ukuran populasi total disediakan, gunakan koreksi populasi finit
    if ($N !== null) {
        $n = $n0 / (1 + ($n0 - 1) / $N);
        return round($n);
    }

    return round($n0);
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ingredient_nutrition_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ingredient_nutrition_id')->constrained('ingredient_nutritions')->cascadeOnDelete();
            $table->integer('attr_id');
            $table->double('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ingredient_nutrition_details');
    }
};

import React from 'react';
import { createStyles, Navbar, Group, Code, Text, Box, Menu, UnstyledButton, NavLink } from '@mantine/core';
import {
    IconHome,
    IconUser,
    IconSettings,
    IconLogout,
    IconChevronDown,
    IconFile,
    IconFolder,
    IconGauge,
    IconFingerprint,
} from '@tabler/icons-react';

const useStyles = createStyles((theme) => ({
    navbar: {
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
        paddingBottom: 0,
    },
    header: {
        padding: theme.spacing.md,
        marginBottom: theme.spacing.md,
        borderBottom: `1px solid ${theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
            }`,
    },
    links: {
        padding: theme.spacing.md,
        paddingTop: 0,
        paddingBottom: theme.spacing.md,
        display: 'flex',
        flexDirection: 'column',
        gap: theme.spacing.xs,
    },
    link: {
        ...theme.fn.focusStyles(),
        display: 'flex',
        alignItems: 'center',
        textDecoration: 'none',
        color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.colors.gray[7],
        padding: `${theme.spacing.sm}px ${theme.spacing.md}px`,
        borderRadius: theme.radius.sm,
        fontSize: theme.fontSizes.md,
        fontWeight: 500,

        '&:hover': {
            backgroundColor:
                theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
            color: theme.colorScheme === 'dark' ? theme.white : theme.black,
        },
    },
    footer: {
        borderTop: `1px solid ${theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
            }`,
        paddingTop: theme.spacing.md,
    },
    submenuLink: {
        display: 'flex',
        alignItems: 'center',
        textDecoration: 'none',
        color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.colors.gray[7],
        padding: `${theme.spacing.xs}px ${theme.spacing.sm}px`,
        borderRadius: theme.radius.sm,
        fontSize: theme.fontSizes.sm,

        '&:hover': {
            backgroundColor:
                theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
            color: theme.colorScheme === 'dark' ? theme.white : theme.black,
        },
    },
}));

const links = [
    { icon: IconHome, label: 'Home', path: '/' },
    {
        icon: IconUser, label: 'Profile', path: '/profile', submenu: [
            { label: 'View Profile', path: '/profile/view' },
            { label: 'Edit Profile', path: '/profile/edit' },
        ],
    },
    { icon: IconSettings, label: 'Settings', path: '/settings' },
];

export default function AppNavbar() {
    const { classes } = useStyles();

    return (
        <Navbar height="100vh" width={{ sm: 300 }}>
            <Navbar.Section grow>
                <NavLink
                    active={location.pathname === '/dashboard'}
                    label="Dashboard"
                    icon={<IconGauge size="1rem" stroke={1.5} />}
                    p={20}
                    childrenOffset={28}
                >
                    <NavLink label="First child link" />
                    <NavLink label="Second child link" />
                    <NavLink
                        label="Nested parent link"
                        childrenOffset={28}
                        active={location.pathname.startsWith('/first-parent/nested')}
                    >
                        <NavLink
                            label="First child link"
                            active={location.pathname === '/first-parent/nested/first-child'}
                        />
                        <NavLink
                            label="Second child link"
                            active={location.pathname === '/first-parent/nested/second-child'}
                        />
                        <NavLink
                            label="Third child link"
                            active={location.pathname === '/first-parent/nested/third-child'}
                        />
                    </NavLink>
                </NavLink>

                <NavLink
                    label="Second parent link"
                    icon={<IconFingerprint size="1rem" stroke={1.5} />}
                    childrenOffset={28}
                    defaultOpened
                    p={20}
                    active={location.pathname.startsWith('/second-parent')}
                >
                    <NavLink
                        label="First child link"
                        active={location.pathname === '/second-parent/first-child'}
                    />
                    <NavLink
                        label="Second child link"
                        active={location.pathname === '/second-parent/second-child'}
                    />
                    <NavLink
                        label="Third child link"
                        active={location.pathname === '/second-parent/third-child'}
                    />
                </NavLink>
            </Navbar.Section>

            <Navbar.Section>
                <a href="/logout">
                    <IconLogout size={20} />
                    <Box ml="sm">Logout</Box>
                </a>
            </Navbar.Section>
        </Navbar>
    );
}

export const USER = {
  ROLE_ADMIN: 1,
  ROLE_PPO: 2,
  ROLE_KAWIL: 3,
  ROLE_MANAGEMENT: 4,
  ROLE_RND: 5,
  ROLE_VIEW: 6,
  ROLE_JURNAL: 7,
  ROLE_KABAG: 8,
  ROLE_SM: 9,
  ROLE_DIR: 10,
  DIVISON_RND: 'RND',
  POSITION_PM_MANAGER: 'PM Manager',
  POSITION_SN_MANAGER: 'Sn. Manager',
  POSITION_RND: 'RND',
  POSITION_DIRECTOR: 'Director',
  POSITION_MANAGING_DIRECTOR: 'Managing Director',
};

export const LOCATION = {
  EL_PROGRESS_STATUS: 0,
  EL_APPROVED_STATUS: 1,
  EL_REJECTED_STATUS: 2,
};

type MaterialItemCodeType = {
  [key: string]: 'Urea' | 'ZA' | 'DAP' | 'K2SO4' | 'KCl';
};

export const MATERIAL_ITEM_CODE: MaterialItemCodeType = {
  Urea: 'Urea',
  ZA: 'ZA',
  DAP: 'DAP',
  K2SO4: 'K2SO4',
  KCl: 'KCl',
};

export const MATERIAL_HEADERS = [
  { material_name: 'Urea', item_code: '' },
  { material_name: 'Urease', item_code: '' },
  { material_name: 'ZA', item_code: '' },
  { material_name: 'K2SO4', item_code: '' },
  { material_name: 'KCl', item_code: '' },
  { material_name: 'Kompos', item_code: '' },
  { material_name: 'Calcibor', item_code: '' },
  { material_name: 'Urea Phosphate', item_code: '' },
  { material_name: 'TSP', item_code: '' },
  { material_name: 'DAP', item_code: '' },
  { material_name: 'MgSO4', item_code: '' },
  { material_name: 'Kieserite Crystal', item_code: '' },
  { material_name: 'FeSO4', item_code: '' },
  { material_name: 'ZnSO4', item_code: '' },
  { material_name: 'Dolomit', item_code: '' },
  { material_name: 'Petro Cas', item_code: '' },
  { material_name: 'Belerang', item_code: '' },
  { material_name: 'Borax', item_code: '' },
  { material_name: 'LOB', item_code: '' },
  { material_name: 'S14', item_code: '' },
  { material_name: 'Bromacyl', item_code: '' },
  { material_name: 'Diuron', item_code: '' },
  { material_name: 'Glyphosate', item_code: '' },
  { material_name: 'Quizalofop P-Ethyl', item_code: '' },
  { material_name: 'Ametryn', item_code: '' },
  { material_name: 'Tekasi', item_code: '' },
  { material_name: 'Tekila', item_code: '' },
  { material_name: 'Dementhoate', item_code: '' },
  { material_name: 'Diazinon', item_code: '' },
  { material_name: 'Propoxur', item_code: '' },
  { material_name: 'Cypermethrin', item_code: '' },
  { material_name: 'Bifenthrin EC', item_code: '' },
  { material_name: 'Bifenthrin G', item_code: '' },
  { material_name: 'Fosetyl-Al', item_code: '' },
  { material_name: 'Metalaxyl', item_code: '' },
  { material_name: 'Benzalkonium Chloride', item_code: '' },
  { material_name: 'Rabas', item_code: '' },
  { material_name: 'Ethylene Gas', item_code: '' },
  { material_name: 'Ethepon', item_code: '' },
  { material_name: 'Organosilicon', item_code: '' },
  { material_name: 'Indostick', item_code: '' },
  { material_name: 'Kaolin', item_code: '' },
];

export const IMAGE_BASE_URL = import.meta.env?.IMAGE_BASE_URL || '';

export const IMAGE_BASE_URL_PIS =
  import.meta.env?.IMAGE_BASE_URL_PIS || 'https://ai.gg-foods.com/PIS/assets/upload/gis_pict';

export const IMAGE_BASE_URL_AWS =
  import.meta.env?.IMAGE_BASE_URL_AWS ||
  'https://aid-digital-innovation.s3.ap-southeast-1.amazonaws.com/live_gis_pict_compress';

export const COMMODITIES = [
  { label: 'Nanas', value: 'NS' },
  { label: 'Pisang', value: 'PS' },
  { label: 'Singkong', value: 'SK' },
  { label: 'Pepaya', value: 'PY' },
];

export const ACCELERATION_COLOR = {
  'akselerasi besar 9 bulan': { bg: 'rgb(187, 255, 90, 0.30)', color: '#456d0d' },
  'akselerasi besar 8 bulan': { bg: 'rgb(190, 75, 219, 0.20)', color: '#87289f' },
  'akselerasi besar 7 bulan': { bg: 'rgb(152, 68, 1, 0.10)', color: '#884100' },
  'akselerasi kecil 13 bulan': { bg: 'rgb(187, 255, 90, 0.30)', color: '#456d0d' },
  'akselerasi kecil 12 bulan': { bg: 'rgb(190, 75, 219, 0.20)', color: '#87289f' },
  'akselerasi kecil 11 bulan': { bg: 'rgb(152, 68, 1, 0.10)', color: '#884100' },
  'akselerasi sedang 11 bulan': { bg: 'rgb(187, 255, 90, 0.30)', color: '#456d0d' },
  'akselerasi sedang 10 bulan': { bg: 'rgb(190, 75, 219, 0.20)', color: '#87289f' },
  'akselerasi sedang 9 bulan': { bg: 'rgb(152, 68, 1, 0.10)', color: '#884100' },
};

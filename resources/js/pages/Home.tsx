import React from 'react';
import { Text } from '@mantine/core';

const Home: React.FC = () => {
    return (
        <div>
            <Text size="xl">Welcome to Home Page</Text>
        </div>
    );
};

export default Home;

import React from 'react';
import ReactDOM from 'react-dom/client';
import { RecoilRoot } from 'recoil';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { AppShell } from '@mantine/core';
import Sidebar from './components/DoubleNavbar';
import Topbar from './components/Topbar';
import Footer from './components/Footer';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';
import RecipeDetail from './pages/Recipes/RecipeDetail';
import Recipes from './pages/Recipes/Recipes';
// import NotFound from './pages/NotFound';

const DashboardLayout: React.FC = () => (
    <AppShell
        padding="md"
        navbar={<Sidebar />}
        header={<Topbar />}
        footer={<Footer />}
        styles={(theme) => ({
            main: { backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0] },
        })}
    >
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/contact" element={<Contact />} />
        </Routes>
    </AppShell>
);

const RecipeDetailLayout: React.FC = () => (
    <RecipeDetail />
);
const RecipesLayout: React.FC = () => (
    <Recipes />
);

ReactDOM.createRoot(document.getElementById('app') as HTMLElement).render(
    <React.StrictMode>
        <RecoilRoot>
            <Router>
                <Routes>
                    <Route path="/dashboard" element={<DashboardLayout />} />
                    <Route path="/" element={<RecipesLayout />} />
                    <Route path="/:id" element={<RecipeDetailLayout />} />
                    {/* <Route path="*" element={<NotFound />} /> */}
                </Routes>
            </Router>
        </RecoilRoot>
    </React.StrictMode>
);

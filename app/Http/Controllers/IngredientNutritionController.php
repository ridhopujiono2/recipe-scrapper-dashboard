<?php

namespace App\Http\Controllers;

use App\Jobs\ExportAnalyzedIngredientsJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class IngredientNutritionController extends Controller
{
    public function showNutritions($id): JsonResponse
    {
        $query = DB::table('ingredients as i')
            ->join('ingredient_nutritions as ins', 'i.id', '=', 'ins.ingredient_id')
            ->join('ingredient_nutrition_details as nd', 'ins.id', '=', 'nd.ingredient_nutrition_id')
            ->join('master_nutritions as mn', 'nd.attr_id', '=', 'mn.attr_id')
            ->select('mn.nutrition_name', DB::raw('SUM(nd.value) as total_nutrition_value'))
            ->where('i.id', $id)
            ->groupBy('mn.nutrition_name')
            ->get();

        return new JsonResponse([
            'data' => $query
        ]);
    }
    public function showNutritionTotal($recipeId): JsonResponse
    {
        $query = DB::table('recipes as r')
            ->join('ingredients as i', 'r.id', '=', 'i.recipe_id')
            ->join('ingredient_nutritions as ins', 'i.id', '=', 'ins.ingredient_id')
            ->join('ingredient_nutrition_details as nd', 'ins.id', '=', 'nd.ingredient_nutrition_id')
            ->join('master_nutritions as mn', 'nd.attr_id', '=', 'mn.attr_id')
            ->select('mn.nutrition_name', 'mn.unit', DB::raw('SUM(nd.value) as total_nutrition_value'))
            ->where('r.id', '=', $recipeId)
            ->groupBy('mn.nutrition_name', 'mn.unit')
            ->get();

        return new JsonResponse([
            'data' => $query
        ]);
    }
    public function showDetailNutrition(Request $request)
    {
        $ingredientId = $request->ingredient_ids;
        ExportAnalyzedIngredientsJob::dispatch($ingredientId);
    }
}

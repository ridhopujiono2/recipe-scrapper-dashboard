<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Recipe;
use Illuminate\Support\Facades\Http;

class ScrapeRecipe implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $site;
    protected $link;
    protected $latestScrapedId;
    protected $latestScrapedPage;
    protected $maxLimit;
    protected $maxThread;

    public function __construct($site, $link, $latestScrapedId = null, $latestScrapedPage = null, $maxLimit, $maxThread)
    {
        $this->site = $site;
        $this->link = $link;
        $this->latestScrapedId = $latestScrapedId;
        $this->latestScrapedPage = $latestScrapedPage;
        $this->maxLimit = $maxLimit;
        $this->maxThread = $maxThread;
    }

    public function handle()
    {
        Http::timeout(7200)->get(
            config('services.api_scraping.url') . "/recipes",
            [
                'site' => $this->site,
                'link' => $this->link,
                'latest_scraped_id' => $this->latestScrapedId,
                'latest_scraped_page' => $this->latestScrapedPage,
                'max_limit' => $this->maxLimit,
                'max_thread' => $this->maxThread,
            ]
        );
    }
}

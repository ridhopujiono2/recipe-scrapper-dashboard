<?php

namespace App\Console\Commands;

use App\Jobs\SyncSampledRecipeIngredientNutritionJob;
use Illuminate\Console\Command;

class SyncSampledRecipeIngredientNutritionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:analyses-nutrition-sampled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analyses Nutrition';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        SyncSampledRecipeIngredientNutritionJob::dispatch();
    }
}

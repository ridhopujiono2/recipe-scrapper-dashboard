<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recipe;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\ScrapingLog;
use App\Models\Step;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $query = Recipe::query()
            ->when($request->recipe_name, fn ($query) => $query->where('title', 'like', '%' . $request->recipe_name . '%'));
        if ($request->paginate) {
            return new JsonResponse([
                'data' => $query->paginate($request->paginate) ?? []
            ]);
        }
        return new JsonResponse([
            'data' => $query->get()
        ]);
    }
    public function show($id): JsonResponse
    {
        $query = Recipe::with(['ingredients', 'steps', 'categories'])->find($id);
        return new JsonResponse([
            'data' => $query
        ]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|unique:recipes',
            'source' => 'required|unique:recipes'
        ]);

        try {
            DB::beginTransaction();

            // Membuat atau memperbarui resep
            $recipe = Recipe::updateOrCreate(
                [
                    'code' => $request->code,
                    'source' => $request->source,
                ],
                [
                    'title' => $request->title,
                    'code' => $request->code,
                    'source' => $request->source,
                    'page' => $request->page,
                    'author' => $request->author,
                    'likes' => intval($request->likes),
                    'image' => $request->image,
                    'created_on_website_at' => Date::parse($request->created_on_website_at)->format('Y-m-d H:i:s'),
                    'last_scraped_at' => $request->start_date
                ]
            );

            ScrapingLog::create([
                'recipe_id' => $recipe->id,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'thread' => $request->thread,
                'batch_id' => $request->batch_id,
                'status' => 'success',
                'error_message' => null
            ]);

            // Menyimpan atau memperbarui kategori berdasarkan source dan judul
            $categoryIds = [];
            foreach ($request->categories as $cat) {
                $category = Category::updateOrCreate(
                    [
                        'link' => $cat['link'],
                        'title' => $cat['title']
                    ]
                );
                $categoryIds[] = $category->id;
            }
            $recipe->categories()->syncWithoutDetaching($categoryIds);

            // Menyimpan atau memperbarui bahan
            $ingredientsData = [];
            foreach ($request->ingredients as $group) {
                foreach ($group['data'] as $item) {
                    $ingredientsData[] = [
                        'recipe_id' => $recipe->id,
                        'title' => $group['title'],
                        'value' => $item['value'],
                        'unit' => $item['unit'],
                        'description' => $item['description'],
                    ];
                }
            }
            Ingredient::upsert($ingredientsData, ['recipe_id', 'title', 'value', 'unit', 'description']);

            // Menyimpan atau memperbarui langkah-langkah
            $stepsData = [];
            foreach ($request->steps as $group) {
                foreach ($group['data'] as $item) {
                    $stepsData[] = [
                        'recipe_id' => $recipe->id,
                        'title' => $group['title'],
                        'step_description' => $item['step'],
                        'image' => $item['image'] ?? null,
                    ];
                }
            }
            Step::upsert($stepsData, ['recipe_id', 'title', 'step_description']);

            DB::commit();
            return response()->json(['message' => 'Recipe saved successfully!'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}

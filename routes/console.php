<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schedule;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote')->hourly();

// Set to scrape resepmamiku.com at 2:00 AM
Schedule::command('scrape:daily resepmamiku.com 1000 20')->dailyAt('02:00');

// Set to scrape masakapahariini.com at 4:00 AM
// Schedule::command('scrape:daily masakapahariini.com 1200 20')->dailyAt('04:00');

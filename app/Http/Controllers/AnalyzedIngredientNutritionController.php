<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class AnalyzedIngredientNutritionController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $query = DB::table('recipes')
            ->join('ingredients', 'recipes.id', '=', 'ingredients.recipe_id')
            ->select(
                'recipes.id',
                'recipes.title',
                'recipes.source',
                DB::raw('COUNT(ingredients.id) AS total_ingredients'),
                DB::raw('SUM(CASE WHEN ingredients.analyzed THEN 1 ELSE 0 END) AS analyzed_ingredients'),
                DB::raw('(SUM(CASE WHEN ingredients.analyzed THEN 1 ELSE 0 END) / COUNT(ingredients.id) * 100) AS percentage_analyzed')
            )
            ->groupBy('recipes.id', 'recipes.title', 'recipes.source');

        if ($request->paginate) {
            return new JsonResponse([
                'data' => $query->paginate($request->paginate) ?? []
            ]);
        }
        return new JsonResponse([
            'data' => $query->get()
        ]);
    }
}
